terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "5.30.0"
    }
  }
}

provider "google" {
  #Configuration options
  project = "devops-kubernetes-gke"
  region = "us-central1"
  credentials = "key.json"
}
