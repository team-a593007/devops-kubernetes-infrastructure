# CI/CD for React Application

![Static Badge](https://img.shields.io/badge/NodeJS-black?style=for-the-badge&logo=nodedotjs&logoColor=green)
![Static Badge](https://img.shields.io/badge/ReactJS-black?style=for-the-badge&logo=react&logoColor=blue)
![Static Badge](https://img.shields.io/badge/Docker-black?style=for-the-badge&logo=docker&logoColor=blue)
![Static Badge](https://img.shields.io/badge/Terraform-black?style=for-the-badge&logo=terraform&logoColor=blue)
![Static Badge](https://img.shields.io/badge/Google%20Cloud%20Plataform-black?style=for-the-badge&logo=googlecloud&logoColor=blue)
[![Static Badge](https://img.shields.io/badge/Kubernetes-black?style=for-the-badge&logo=kubernetes&logoColor=blue)](https://img.shields.io/badge/Kubernetes-black?style=for-the-badge&logo=kubernetes&logoColor=blue)

Este repositório possui um pipeline de CI/CD utilizando Docker e Kubernetes, implementando diversos conceitos de Devops composto por 3 steps: 

1. Realiza o build do código fonte de uma aplicação React dentro da pipeline assim que é realizado um push

2. Após o build a pipeline cria uma imagem Docker com um arquivo loader caso necessário um teste de estresse simulando múltiplos acessos de diferentes usuários em um curto período de tempo.

3. Por fim faz o pull da imagem para o cluster Kubernetes com um mestre e diferentes worker nodes junto com um load balance para lidar com múltiplos acessos e o risco de perda de dados.

<br>

A aplicação é uma simples calculadora construída em React, utilizando Tailwindcss para sua estilização, para disponibilizar a aplicação como website na Internet para navegação HTTP foi utilizado o Apache.

Essa aplicação está disponível no [Docker Hub](https://hub.docker.com/r/victorfy/react-app) como imagem para utilização em container.

Abaixo se encontra um diagrama básico destacando o funcionamento de todo o pipeline e da orquestração realizada pelo Kubernetes.

![Diagrama Devops](./.gitlab/gitlab_devops.png)

Dentro do cluster criado pelo Kubernetes é realizado o load balance como demonstrado no driagrama a seguir:

![Load Balance](./.gitlab/load-balance.png)

A cultura do Devops pode aumentar a capacidade das empresas em desenvolver e distribuir aplicativos ou serviços em alta velocidade, atendendo melhor os seus clientes. Ou seja, o Devops é intimamente ligado as metodolodias ágeis que são amplamente difundidas atualmente.

A utilização do Docker para criar containers em conjunto com o Kubernetes para lidar com a orquestração dos containers criando clusters (conjunto de servidores interconectados, que atuam como se fossem um único sistema e trabalham juntos para realizar tarefas de forma mais eficiente e escalável), garantem boa performace (que fica sob a responsabilidade de um load balancer), adiciona escabilidade, segurança e robustes na infraestrutura, entregando uma otimização dos recursos, bom tempo de resposta em caso de múltiplos acessos em curtos períodos de tempo e não ocorrência de downtime.

O Kubernetes permite que sejam criados ambientes isolados do cluster para integração, entrega contínua, ou até mesmo para experimentar novas features; permite também escalonamento dinâmico das aplicações, facilitando que sejam criados mais containers rapidamente para atender as demandas de acordo com os recursos disponíveis.

Esses fatores tornam possível a entrega contínua e frequente de novas versões, o que se encaixa com as metodologias ágeis.
